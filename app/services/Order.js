app.factory('Order', ['DB', function(DB)
{
	DB.setTable("orders", {
		customer: null,
		products: []
	});

	this.rules = {
		customer: 'required|alpha_num'
	}

	return _.extend(DB, this);
}]);