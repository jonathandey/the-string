app.factory('DB', ['$localStorage', '$window', 'Util', function($localStorage, $window, Util)
{
	var c = this;

	if($localStorage.db == undefined)
	{
		$localStorage.db = [];
	}

	c.setTable = function(tableName, columns)
	{
		c.table = _.find($localStorage.db, function(table){ var keys = Object.keys(table); return keys[0] == tableName; });
		c.tableName = tableName;
		c.customColumns = columns;

		if(!c.table)
		{
			var obj = {};
			obj[tableName] = [];
			var newTable = $localStorage.db.push(obj);

			c.table = _.find($localStorage.db, function(table){ var keys = Object.keys(table); return keys[0] == tableName; });
		}
	}

	c.reset = function()
	{
		var defaultColumns = {
			id: null,
			created_at: null,
			updated_at: null,
			deleted_at: null
		}
		
		return _.extend(defaultColumns, c.customColumns);
	}

	c.use = function(mix)
	{
		c.val = mix;

		return c;
	}

	c.where = function(search)
	{
		c.val = _.where(c.all(), search);

		return c;
	}

	c.filter = function(conditions)
	{
		c.val = _.filter(c.all(), conditions);

		return c;
	}

	c.first = function()
	{
		if(_.isArray(c.val))
		{
			return c.val[0];
		}

		return c.val;
	}

	c.pluck = function(col)
	{
		return _.pluck(c.val, col);
	}

	c.get = function()
	{
		if(!_.isNull(c.val))
		{
			return c.val;
		}

		return c.all();
	}

	c.find = function(id)
	{
		return _.find(c.all(), function(obj){ return obj.id == id });
	}

	c.max = function(paramFunction)
	{
		if(!_.isNull(c.val))
		{
			return _.max(c.val, paramFunction);
		}

		return _.max(c.all(), paramFunction);
	}

	c.min = function(paramFunction)
	{
		if(!_.isNull(c.val))
		{
			return _.min(c.val, paramFunction);
		}

		return _.min(c.all(), paramFunction);
	}

	c.groupBy = function(paramFunction)
	{
		if(!_.isNull(c.val))
		{
			return _.groupBy(c.val, paramFunction);
		}

		return _.groupBy(c.all(), paramFunction);
	}

	c.sortBy = function(paramFunction)
	{
		if(!_.isNull(c.val))
		{
			return _.sortBy(c.val, paramFunction);
		}

		return _.sortBy(c.all(), paramFunction);
	}

	c.count = function()
	{
		if(!_.isNull(c.val))
		{
			return c.val.length;
		}
		
		return c.all().length;
	}

	c.uniqueId = function()
	{
		return Util.uniqueId();
	}

	/**
	* Actions
	*
	*/
	c.all = function()
	{
		return c.table[c.tableName];
	}

	c.validate = function(input){
		var validation = new Validator(input, c.rules);

		if(validation.fails())
		{
			return validation;
		}

		return true;
	}

	c.create = function(passedObj)
	{
		var timestamp = Date.now();

		passedObj.id = c.uniqueId();
		passedObj.created_at = timestamp;
		passedObj.updated_at = timestamp;

		c.table[c.tableName].push(passedObj);
		return passedObj;
	}

	c.save = function(passedObj)
	{
		if(_.isNull(passedObj.id))
		{
			return c.create(passedObj);
		}

		var newStorage = _.reject(c.all(), function(obj){ return obj == passedObj });
		passedObj.updated_at = Date.now();
		newStorage.push(passedObj);

		return c.table[c.tableName] = newStorage;
	}

	c.empty = function()
	{
		if($window.confirm('Are you sure you\'d like to delete all items?'))
		{
			return c.table[c.tableName] = [];
		}
	}

	c.delete = function()
	{
		var updated = _.reject(c.all(), function(obj){ return obj.id == c.val.id });

		return c.table[c.tableName] = updated;
	}

	return c;
}]);