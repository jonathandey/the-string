app.factory('Product', ['DB', function(DB)
{
	DB.setTable("products", {
		name: null,
		price: null
	});

	this.rules = {
		name: 'required|alpha',
		price: 'required|numeric'
	}

	return _.extend(DB, this);
}]);