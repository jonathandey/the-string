app.controller('ProductCtrl', ['$scope', 'Product', function($scope, Product){
	
	/* Issue: after editing then readding an item it isn't appended to the list */
	$scope.inputs = Product.reset();
	$scope.items = Product.all();

	$scope.actions = {
		save: function()
		{
			var validate = Product.validate($scope.inputs);

			if(validate == true)
			{
				Product.save($scope.inputs);
				$scope.items = Product.all();
				$scope.inputs = Product.reset();

				$scope.errors = false;
				$scope.success = "Product saved.";
				
				return true;
			}

			$scope.success = false;
			$scope.errors = validate.errors;
		},
		edit: function(itemId)
		{
			$scope.inputs = Product.find(itemId);
		},
		remove: function(itemId)
		{
			$scope.items = Product.use(Product.find(itemId)).delete();
		},
		removeAll: function()
		{
			$scope.items = Product.empty();
		},
	}	
}]);