app.controller('OrderCtrl', ['$scope', 'Order', function($scope, Order){
	
	$scope.inputs = Order.reset();
	$scope.items = Order.all();

	$scope.actions = {
		save: function()
		{
			var validate = Order.validate($scope.inputs);

			if(validate == true)
			{
				Order.save($scope.inputs);
				$scope.items = Order.all();
				$scope.inputs = Order.reset();

				$scope.errors = false;
				$scope.success = "Order saved.";
				
				return true;
			}

			$scope.success = false;
			$scope.errors = validate.errors;
		},
		edit: function(itemId)
		{
			$scope.inputs = Order.find(itemId);
		},
		remove: function(itemId)
		{
			$scope.items = Order.use(Order.find(itemId)).delete();
		},
		removeAll: function()
		{
			$scope.items = Order.empty();
		},
	}	
}]);