app.config(['$routeProvider', function($routeProvider)
{
	$routeProvider
	.when('/', {
		templateUrl: 'app/views/home/index.html',
		controller: 'HomeCtrl'
	})
	.when('/product', {
		templateUrl: 'app/views/product/create.html',
		controller: 'ProductCtrl'
	})
	.when('/order', {
		templateUrl: 'app/views/order/create.html',
		controller: 'OrderCtrl'
	});
}]);