app.directive('save', function()
{
	return {
		restrict: 'E',
		transclude: true,
		template: '<button class="btn btn-lg btn-primary btn-block" ng-click="actions.save()">Save</button>',
		link: function(scope, e, attrs) 
		{
			if(attrs.type)
			{
				e.children('button')[0].innerHTML = 'Save ' + attrs.type;
			}
		}
	}
});

app.directive('edit', function()
{
	return {
		restrict: 'E',
		transclude: true,
		template: '<a class="btn" ng-click="actions.edit(item.id)">Edit</a>',
		link: function(scope, e, attrs) {}
	}
});

app.directive('remove', function()
{
	return {
		restrict: 'E',
		transclude: true,
		template: '<a class="btn" ng-click="actions.remove(item.id)">Remove</a>',
		link: function(scope, e, attrs) {}
	}
});