app.factory('Util', function()
{
	return {
		uniqueId: function(pattern)
		{
			var pattern = (pattern == undefined) ? 'yxxyyxy' : pattern,
			uniqueId = '';

			uniqueId += pattern.replace(/[xy]/g, function(c) {
			    var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
			    return v.toString(16);
			});

			return uniqueId;
		}
	}
});